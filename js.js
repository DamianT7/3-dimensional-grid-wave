//2D rotation function
function r2d(x,z,r) {
    var c=Math.cos(r);
    var s=Math.sin(r);
    return [x*c-z*s,x*s+z*c];
}

//3D rotation function
function r3d(x,r) {
    var t0=r2d(x[0],x[2],r[1]);
    var t1=r2d(x[1],t0[1],r[0]);
    var t2=r2d(t1[0],t0[0],r[2]);
    return [t2[1],t2[0],t1[1]];
}

//3D to 2D conversion/projection
function proj(x) {
    return [innerWidth/2+x[0]/x[2]*sca,innerHeight/2+x[1]/x[2]*sca];
}

//Total 3D conversion/projection with 3D rotation
function t3d(x,r) {
    var t0=r3d(r3d(x,r).map((a,b)=>a+cpos[b]),crot);
    if(t0[2]<=0&&t0[2]>=-5) t0[2]=1e-6;
    return t0;
}

//Camera settings
var cpos=[0,2,4];
var crot=[.5,0,0];

//Total Object Rotation
var tor=[0,0,0];

//Scaling factor
var sca;

//Vertices
var vrts=[

];

//Lines
var lns=[

];

//Generating vertices for the grid
var gridsize=5;
var holesize=Math.pow(.5,2);
for(var x=-gridsize/2;x<=gridsize/2;x+=holesize) {
    for(var z=-gridsize/2;z<=gridsize/2;z+=holesize) {
        vrts.push([x,1,z]);
    }
}

//Generating lines for the grid
for(var i=0;i<vrts.length;i++) {
    if(Math.floor(i/(gridsize/holesize))<=gridsize/holesize)lns.push([i,i+gridsize/holesize+1]);
    if((i+1)%(gridsize/holesize+1)!==0) lns.push([i,i+1]);
}
//Main frame function
function frame() {
    c=document.querySelector("canvas");
    [c.width,c.height]=[innerWidth,innerHeight];
    ctx=c.getContext("2d"); //Initalization
    //-=-//
    tor=[0,Date.now()/2000,0];
    sca=Math.min(innerWidth,innerHeight)/2;
    ctx.strokeStyle="white";
    //Changing the vertices' position
    for(var i=0;i<vrts.length;i++) {
        vrts[i][1]=Math.cos(Date.now()/300+(i%(gridsize/holesize))*holesize*2)*.2+1;
    }

    //Rendering the lines
    for(var i=0;i<lns.length;i++) {
        var cp0=t3d(vrts[lns[i][0]],tor);
        var cp1=t3d(vrts[lns[i][1]],tor);
        ctx.beginPath();
        ctx.moveTo(...proj(cp0));
        ctx.lineTo(...proj(cp1));
        ctx.stroke();
        ctx.closePath();
    }
}
setInterval(frame);